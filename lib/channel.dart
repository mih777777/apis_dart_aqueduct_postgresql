import 'dart:io';
import 'package:restapp/controller/reads_controller.dart';
import 'restapp.dart';

class FaveReadsChannel extends ApplicationChannel {
  ManagedContext context;


  @override
  Future prepare() async {
    logger.onRecord.listen(
        (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));

    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
      'api_dart_user',
      '1',
      'localhost',
       5432,
      'api_dart'
    );

    context = ManagedContext(dataModel, persistentStore);
  }

  
  @override
  Controller get entryPoint => Router()
    ..route("/reads/[:id]").link(() => ReadsController(context))
    
    ..route('/').linkFunction((request) =>
        Response.ok('Hello, World!')..contentType = ContentType.html)
    
    ..route('/client').linkFunction((request) async {
      final client = await File('client.html').readAsString();
      return Response.ok(client)..contentType = ContentType.html;
    });
}
